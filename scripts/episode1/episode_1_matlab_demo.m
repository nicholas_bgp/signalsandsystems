clear;
close all;

t = linspace(-6, 6, 1000);
x = sin(2*pi/4*t);

% (a)
y1 = [];
for k = 1:2:numel(x)
  y1 = [x(k) y1];
end
y1 = repmat(y1, [1 2]);

% (b)
y2_ideal = sin(2*pi/4*(.5*t));
y2 = [];
for k = 1:.5:numel(x)
  if rem(k,1) == 0
    y2 = [x(k) y2];
  else
    y2 = [0 y2];
  end
end
y2 = [0 y2];
y2 = -1*y2(numel(x)/2+1:1.5*numel(x));

% (c)
y3 = [];
for k = 1:numel(x)
end