## Episode 1 Figures
# Nicholas McKibben
# 2017-05-01

clear;
close all;

# Continuous time
t = linspace(-5, 5, 1000);
x_t = sinc(t);
figure(1);
plot(t, x_t);
title('Continuous Time Signal x(t)');
xlabel('Time, t');
ylabel('Magnitude, x');
xlim([ -5 5 ]);

# Discrete time
n = 0:10;
x_n = exp(-n/pi);
figure(2);
stem(n, x_n);
title('Discrete Time Signal x[n]');
xlabel('Sample, n');
ylabel('Magnitude, x');