## Signals and Systems

So the idea is to create short educational videos (similar in style to those of Crash Course) for Signal Processing,
or at least the basics of it, like basic Signals and Systems and core DSP Principles that I know and understand well.
It would also help me to cement in what I already know and allow me to examine critically that which I didn't
understand very well or didn't know that I didn't undestand very well.

So, in creating this YouTube course, here's the line up (based on my own ECEn 380 experience):

1. Introduction to the Course Introduction to Signals
2. Types and Signals and Transformations
3. Waveform properties and non-periodic waveforms
4. (An aside on teaching and learning math in engineering programs)
5. Signal Power and Energy
6. LTI/LSI Systems
7. Impulse Response
8. Convolution (analytic and graphical)
9. Convolution Properties, Causality, and BIBO Stability
10. LTI Sinusoidal Response, Impulse response of 2nd Order LCCDEs
11. Unilateral Lapplace Transform, Poles and Zeros
12. Properties of the Laplace Transform
13. Partial Fraction Expansion
14. Transfer Functions, Poles and System Stability
15. Invertible Systems
And More... (they just weren't specified on the schedule I looked at...)